import org.apache.maven.model.ReportSet;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

@Mojo(name = "moderate",
        executionStrategy = "always",
        threadSafe = true,
        defaultPhase = LifecyclePhase.PROCESS_SOURCES)
public class BadWordsMojo extends AbstractMojo {

    @Parameter(property = "words", required = true)
    String[] words;

    @Parameter(property = "directory", defaultValue = "${project.build.sourceDirectory}")
    File directory;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        StringBuilder reports = new StringBuilder();
        try {
            Files.walkFileTree(Paths.get(directory.toURI()), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    FileVisitResult visitResult = super.visitFile(file, attrs);

                    List<String> fileContent = Files.readAllLines(file);
                    for (String line : fileContent) {
                        for (String word : words) {
                            if (line.contains(word)) {
                                reports.append(word)
                                        .append("\n");
                            }
                        }
                    }
                    return visitResult;
                }
            });
        } catch (IOException e) {
            throw new MojoExecutionException("Exception during parsing", e);
        }
        if (!reports.isEmpty()) {
            throw new MojoFailureException("Found bad words: " + reports);
        }
    }
}
