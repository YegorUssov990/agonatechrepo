package com.technokratos.jdbc;

import com.technokratos.jdbc.model.Account;
import com.technokratos.jdbc.repository.AccountRepository;
import com.technokratos.jdbc.repository.AccountRepositoryJdbcImpl;
import com.technokratos.jdbc.repository.CrudRepository;
import org.postgresql.jdbc2.optional.SimpleDataSource;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

public class Main {

    public static void main(String[] args) throws SQLException, FileNotFoundException {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("/Users/daniilbogomolov/Desktop/agona/jdbc-example/src/main/resources/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        Connection connection =
                DriverManager.getConnection(
                        properties.getProperty("db.url"),
                        properties.getProperty("db.username"),
                        properties.getProperty("db.password")
                );

        AccountRepository repositoryJdbc = new AccountRepositoryJdbcImpl(connection);
        List<Account> accounts = repositoryJdbc.findAllByFirstName("Daniil'; DROP TABLE victim; -- or 1 = 1; --");
//        select * from account where first_name = 'Daniil' or 1 = 1; --'    ----- statement
//        select * from account where first_name = 'Daniil''; DROP TABLE victim; -- or 1 = 1; --'
        int i = 0;

//        Statement statement = connection.createStatement();
//        statement.execute("INSERT into account(id, first_name, role) " +
//                "values ('43c130a3-b6fd-4f33-8ad3-a12041bb7187', 'Daniil', 'ADMIN')");
//        ResultSet resultSet = statement.executeQuery("SELECT * FROM account");
//        while (resultSet.next()) {
//            String id = resultSet.getString("id");
//            String firstName = resultSet.getString("first_name");
//            String role = resultSet.getString("role");
//            System.out.println(
//                    String.format("%s %s %s", id, firstName, role)
//            );
//        }
//        connection.close();
    }

//      id    | first_name | role        0
//     -----------------------------
//      1     |   Daniil  |  USER        1
//      2     |   Daniil2 |  ADMIN       2
//
}
