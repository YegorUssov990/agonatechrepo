package com.technokratos.jdbc.model;

import java.util.UUID;

public class Account {

    private UUID id;
    private String firstName;
    private Role role;
    private Device lastLoggedDevice;

    public Account(UUID id, String firstName, Role role) {
        this.id = id;
        this.firstName = firstName;
        this.role = role;
    }

    public Account(UUID id, String firstName, Role role, Device lastLoggedDevice) {
        this.id = id;
        this.firstName = firstName;
        this.role = role;
        this.lastLoggedDevice = lastLoggedDevice;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
