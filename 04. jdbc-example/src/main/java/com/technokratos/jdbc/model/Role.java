package com.technokratos.jdbc.model;

public enum Role {
    USER, ADMIN
}
