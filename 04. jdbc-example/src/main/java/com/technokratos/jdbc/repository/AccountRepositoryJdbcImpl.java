package com.technokratos.jdbc.repository;

import com.technokratos.jdbc.model.Account;
import com.technokratos.jdbc.model.Device;
import com.technokratos.jdbc.model.Role;

import javax.swing.plaf.nimbus.State;
import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class AccountRepositoryJdbcImpl implements AccountRepository {

    public static final String SQL_FIND_ALL = "SELECT * FROM account";
    public static final String SQL_FIND_ALL_BY_FIRST_NAME = "select * from account where first_name = ?";
    public static final String SQL_FIND_BY_ID = "SELECT * FROM account WHERE id = %s";
    public static final String SQL_FIND_BY_ID_WITH_DEVICE = "SELECT * FROM account INNER JOIN device ON account.device_id = device.id";
    public static final String SQL_INSERT = "INSERT into account(id, first_name, role) values ('%s', '%s', '%s')";

    private static final RowMapper<Account> ACCOUNT_ROW_MAPPER = row -> {
        String id = row.getString("id");
        String firstName = row.getString("first_name");
        String role = row.getString("role");
        return new Account(UUID.fromString(id), firstName, Role.valueOf(role));
    };

    private static final RowMapper<Account> ACCOUNT_WITH_DEVICE_ROW_MAPPER = row -> {
        String id = row.getString("id");
        String firstName = row.getString("first_name");
        String role = row.getString("role");
        Long deviceId = row.getLong("device_id");
        String name = row.getString("name");
        Device device = new Device(deviceId, name);
        return new Account(UUID.fromString(id), firstName, Role.valueOf(role), device);
    };

    private final Connection connection;

    public AccountRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    public UUID create(Account account) {
        try(Statement statement = connection.createStatement()) {
            UUID id = UUID.randomUUID();
            String sql = String.format(SQL_INSERT, id, account.getFirstName(), account.getRole());
            statement.execute(sql);
            return id;
        } catch (SQLException e) {
            throw new RuntimeException("Exception on account findAll", e);
        }
    }

    public Account update(Account account) {
        return account;
    }

    public void deleteById(UUID id) {

    }

    public Optional<Account> findById(UUID accountId) {
        try(Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(String.format(SQL_FIND_BY_ID, accountId));
            if (resultSet.next()) {
                return Optional.ofNullable(ACCOUNT_ROW_MAPPER.mapRow(resultSet));
            }
            return Optional.empty();
        } catch (SQLException e) {
            throw new RuntimeException("Exception on account findAll", e);
        }
    }

    public List<Account> findAll() {
        try(Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(SQL_FIND_BY_ID_WITH_DEVICE);
            List<Account> accounts = new ArrayList<>();
            while (resultSet.next()) {
                Account account = ACCOUNT_WITH_DEVICE_ROW_MAPPER.mapRow(resultSet);
                accounts.add(account);
            }
            return accounts;
        } catch (SQLException e) {
            throw new RuntimeException("Exception on account findAll", e);
        }
    }

    @Override
    public List<Account> findAllByFirstName(String firstName) {
        try(PreparedStatement statement = connection.prepareStatement(SQL_FIND_ALL_BY_FIRST_NAME)) {
            statement.setString(1, firstName);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            List<Account> accounts = new ArrayList<>();
            while (resultSet.next()) {
                Account account = ACCOUNT_ROW_MAPPER.mapRow(resultSet);
                accounts.add(account);
            }
            return accounts;
        } catch (SQLException e) {
            throw new RuntimeException("Exception on account findAll", e);
        }
    }
}
