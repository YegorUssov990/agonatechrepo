package com.technokratos.jdbc.repository;

import com.technokratos.jdbc.model.Account;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CrudRepository<E, ID> {

    ID create(E entity);

    E update(E entity);

    void deleteById(ID id);

    Optional<E> findById(ID id);

    List<E> findAll();
}
