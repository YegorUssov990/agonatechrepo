package com.technokratos.app;

import com.technokratos.model.Course;
import com.technokratos.model.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.UUID;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();



        try (Session session = sessionFactory.openSession()) {

            Course course = session.get(Course.class, UUID.fromString("df2781ca-454b-47ab-bced-f6f74d618a95"));
            course.getStudents().stream().map(Student::getId).forEach(System.out::println);
        }

    }
}
