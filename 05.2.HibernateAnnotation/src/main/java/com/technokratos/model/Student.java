package com.technokratos.model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;


@Entity
@Table(name = "student")
public class Student extends AbstractEntity{

    @Column(name = "name_for", unique = true, nullable = false, length = 50)
    private String nameFor;

    private Integer age;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;
}
