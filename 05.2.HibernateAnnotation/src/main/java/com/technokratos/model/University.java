package com.technokratos.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "university")
public class University extends AbstractEntity {

    private String name;

    @ManyToMany(targetEntity = Course.class)
    @JoinTable(schema = "university_course", )
    private Set<Course> courses;
}
