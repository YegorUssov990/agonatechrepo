package com.technokratos.app;

import com.technokratos.models.Course;
import com.technokratos.models.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session session = sessionFactory.openSession();

        Course course = Course.builder()
                .name("11-901")
                .build();

        Student student = Student.builder()
                .name("Artem")
                .course(course)
                .build();

        session.save(course);
        session.save(student);
        session.close();

    }
}
