package com.technokratos.app;

import com.technokratos.models.Course;
import com.technokratos.repository.CourseRepository;
import com.technokratos.repository.CourseRepositoryImpl;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.TemporalAmount;
import java.util.UUID;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();
        EntityManager entityManager = sessionFactory.createEntityManager();
        CourseRepository coursesRepositoryJpa = new CourseRepositoryImpl(entityManager);

        UUID id = coursesRepositoryJpa.save(
                Course.builder()
                        .title("Super")
                        .startDate(Instant.now())
                        .endDate(Instant.now().plusSeconds(100L))
                        .build()
        );
        System.out.println(coursesRepositoryJpa.findById(id));
    }
}
