package com.technokratos.repository;

import com.technokratos.models.Course;

import java.util.UUID;

public interface CourseRepository extends CrudRepository<Course, UUID> {
}
