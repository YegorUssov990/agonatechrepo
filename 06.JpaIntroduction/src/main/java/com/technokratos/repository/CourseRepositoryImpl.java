package com.technokratos.repository;


import com.technokratos.models.Course;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class CourseRepositoryImpl implements CourseRepository {
    private final EntityManager entityManager;

    public CourseRepositoryImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public UUID save(Course course) {
        EntityTransaction transaction = entityManager.getTransaction();

        transaction.begin();
        entityManager.persist(course);
        transaction.commit();
        return course.getId();
    }

    @Override
    public Optional<Course> findById(UUID uuid) {
        TypedQuery<Course> query = entityManager.createQuery("select course from Course course " +
                "where course.id = :id", Course.class);
        query.setParameter("id", uuid);
        return Optional.ofNullable(query.getSingleResult());
    }

    @Override
    public void delete(Course course) {
        entityManager.remove(course);
    }
}
