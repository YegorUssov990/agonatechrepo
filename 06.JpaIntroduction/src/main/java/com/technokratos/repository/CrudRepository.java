package com.technokratos.repository;

import java.io.Serializable;
import java.util.Optional;

public interface CrudRepository<T, ID extends Serializable> {

    ID save(T t);
    Optional<T> findById(ID id);
    void delete(T t);
}
