package com.technokratos;

import com.technokratos.config.ApplicationConfig;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Locale;

public class Main {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext configurableApplicationContext = new AnnotationConfigApplicationContext();
        configurableApplicationContext.register(ApplicationConfig.class);
        configurableApplicationContext.refresh();
//        UserServlet userServlet = context.getBean(UserServlet.class);
//        UserService userService = context.getBean(UserService.class);
//        UserService userService2 = context.getBean(UserService.class);
//        UserService userService3 = context.getBean(UserService.class);

        MessageSource messageSource = configurableApplicationContext
                .getBean(MessageSource.class);
        String en = messageSource.getMessage("type.some", null, Locale.ENGLISH);
        String ru = messageSource.getMessage("type.some", null, Locale.forLanguageTag("ru"));
        
        int i = 0;
    }
}
