package com.technokratos.config;

import com.technokratos.repository.UserRepository;
import com.technokratos.repository.impl.UserRepositoryJdbcImpl;
import com.technokratos.service.UserService;
import com.technokratos.service.impl.UserServiceImpl;
import com.technokratos.servlet.UserServlet;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan(basePackages = "com.technokratos")
public class ApplicationConfig {

    @Bean
    public UserServlet userServlet(UserService service) {
        return new UserServlet(service);
    }

    @Bean
    public UserService userService(UserRepository repository) {
        return new UserServiceImpl(repository);
    }

    @Bean
    public UserRepository userRepository() {
        return new UserRepositoryJdbcImpl();
    }
}
