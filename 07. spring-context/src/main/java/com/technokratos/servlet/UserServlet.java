package com.technokratos.servlet;

import com.technokratos.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

public class UserServlet {

    private final UserService userService;

    public UserServlet(UserService userService) {
        this.userService = userService;
    }
}
