package com.technokratos.services;

import com.technokratos.dto.UserDto;
import com.technokratos.models.User;

import java.util.List;

public interface UsersService {
    List<User> getAllUsers();
    List<UserDto> getAllUser(int page, int size);
    void addUser(UserDto userDto);
}
