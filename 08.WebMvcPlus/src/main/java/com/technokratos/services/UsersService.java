package com.technokratos.services;

import com.technokratos.dto.UserDto;

import java.util.List;

public interface UsersService {
    List<UserDto> getAllUsers();
    List<UserDto> getAllUsers(int page, int size);
    void addUser(UserDto userDto);

    UserDto getUser(Long userId);
}
