package com.technokratos.util.mapper;

import com.technokratos.model.ApplicationEntity;
import dto.response.ApplicationResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ApplicationMapper {

    @Mapping(target = "name", source = "name", )
    ApplicationResponse toResponse(ApplicationEntity entity);

    List<ApplicationResponse> toResponse(List<ApplicationEntity> entity);
}
