package com.technokratos.rabbitmq.clients;

import com.technokratos.api.UserApi;
import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(name = "userClient", url = "http://localhost:8082")
public interface UserClient extends UserApi {
}
