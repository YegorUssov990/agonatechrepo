package com.technokratos.rabbitmq.config.properties;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@ConfigurationProperties(prefix = "service-rabbit")
@Validated
@Data
public class RabbitProperties {

    @NotNull
    private String routingKey;

    @NotNull
    private String queueName;

    @NotNull
    private String exchangeName;

    @NotNull
    private String dqlQueueName;

    @NotNull
    private String dlqExchangeName;
}
