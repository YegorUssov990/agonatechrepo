package com.technokratos.rabbitmq.controller;

import com.technokratos.rabbitmq.dto.event.User;
import com.technokratos.rabbitmq.service.RabbitProducer;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class MyController {

    private final RabbitProducer producer;

    @PostMapping("/users")
    public void createUser(@RequestBody User user) {
        producer.createUser(user);
    }
}
