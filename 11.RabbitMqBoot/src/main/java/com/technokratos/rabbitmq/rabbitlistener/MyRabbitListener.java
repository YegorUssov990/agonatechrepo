package com.technokratos.rabbitmq.rabbitlistener;

import com.technokratos.rabbitmq.dto.event.User;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class MyRabbitListener {

    @RabbitListener(queues = "${service-rabbit.queue-name}", ackMode = "AUTO")
    public void getMessage(User user) {
        System.out.println(user);
    }
}
