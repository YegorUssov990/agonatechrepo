package com.technokratos.rabbitmq.service;

import com.technokratos.rabbitmq.dto.event.User;

public interface RabbitProducer {
    void createUser(User user);
}
