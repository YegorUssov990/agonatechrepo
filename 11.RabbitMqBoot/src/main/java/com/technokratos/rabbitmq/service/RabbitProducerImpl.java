package com.technokratos.rabbitmq.service;

import com.technokratos.rabbitmq.config.properties.RabbitProperties;
import com.technokratos.rabbitmq.dto.event.User;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class RabbitProducerImpl implements RabbitProducer {

    private final RabbitTemplate template;
    private final RabbitProperties properties;

    @Override
    public void createUser(User user) {
        template.convertAndSend(
                properties.getExchangeName(),
                properties.getRoutingKey(),
                user
        );
    }

}
