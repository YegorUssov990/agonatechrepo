package com.technokratos.rabbitmq.service;

import com.technokratos.dto.request.UserRequest;
import com.technokratos.rabbitmq.clients.UserClient;
import com.technokratos.rabbitmq.dto.event.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserService {

    private final UserClient client;

    public void createUser() {
        client.createUser(new UserRequest());
    }
}
